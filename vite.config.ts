import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path';
import { spawnSync } from 'child_process';
import { execSync } from 'child_process';
import { resolve } from 'path';
import svgr from "vite-plugin-svgr";
import { visualizer } from 'rollup-plugin-visualizer';

function myBeforeCompilePlugin() {

  const runScript = () => {
    const scriptPath = resolve(__dirname, 'beforeCompileScript.js');
    try {
      execSync(`node ${scriptPath}`, { stdio: 'inherit' });
      console.log('目录脚本运行成功！');
    } catch (error) {
      console.error('运行脚本时出错:', error);
    }
  };


  return {
    name: 'my-before-compile-plugin', // 插件名称用于调试和日志

    buildStart() {
      runScript()
    },
    handleHotUpdate({ file }) {
      runScript();
    },
  };
}

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    svgr(),
    myBeforeCompilePlugin(),
    react(),
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
  },
  build: {
    minify: 'esbuild',
    sourcemap: false,
    rollupOptions: {
      output: {
        //分包
        manualChunks: {
          'react-tool': ['react', 'react-router-dom', 'react-dom', '@reduxjs/toolkit', 'react-redux'],
          'tool': ['lodash', 'dayjs', 'axios'],
          'antd': ['antd'],
          'ant-design-icons': ['@ant-design/icons'],
          'react-quill': ['react-quill','quill-table-better'],
          'mathlive': ['mathlive'],
          'html5-qrcode':['html5-qrcode']
        },
      },
    },
  },
})
