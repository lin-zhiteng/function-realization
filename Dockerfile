FROM  node:18.20.4 as builder

# 设置工作目录
WORKDIR /app

COPY package.json pnpm-lock.yaml ./

RUN npm config get registry  

RUN npm config set registry https://registry.npmmirror.com/

RUN npm install -g pnpm

RUN pnpm i


COPY . .

RUN pnpm run build

FROM nginx:latest

COPY --from=builder /app/dist/ /usr/share/nginx/html

EXPOSE 80
