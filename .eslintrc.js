module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  globals: {
    React: true,
    ReactDOM: true,
  },
  plugins: ['react', '@typescript-eslint'],
  extends:[],
  // extends: ['eslint:recommended', 'plugin:react/recommended', 'plugin:@typescript-eslint/recommended','prettier'],
  rules: {
    // "no-console": "error",
  },
};

