import { SkipType } from '@/enum';
import React, { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';

export default function useSkip() {
  const navigate = useNavigate()

  const skipTo = useCallback((type: SkipType) => {
    let path = '';
    let condition = {
      [SkipType.HOME]: '/',
      [SkipType.FUN]: '/funList',
      [SkipType.ABOUT]: '/about',
      [SkipType.ARTICLE]: '/article',
    }
    path = condition[type]
    navigate(path)
  }, [])

  return {
    skipTo
  }
}