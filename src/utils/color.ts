// 引入Color.js库
import Color from 'color';

// 定义一个函数，用于加深颜色
export function darkenColor(color:string, amount:number) {
  // 将十六进制颜色转换为RGB值
  const rgb = Color(color).rgb();

  // console.log(rgb.re);
  

  // 将RGB值转换为HSV值
  const hsv = Color({r: rgb.red(), g: rgb.green(), b: rgb.blue()}).hsv();



  // 增加饱和度和亮度

  // 将HSV值转换为RGB值
  const newRgb = hsv.rgb();

  // 返回新的颜色值
  return newRgb.string();
  
}
