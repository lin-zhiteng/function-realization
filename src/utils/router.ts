/** 跳转新的标签页*/
export function openNewTab(url: string) {
  window.open(url, '_blank');
}