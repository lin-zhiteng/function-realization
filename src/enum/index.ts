enum SkipType{
  HOME="HOME",
  FUN="FUN",
  ABOUT="ABOUT",
  ARTICLE="ARTICLE"
}

export {
  SkipType
}