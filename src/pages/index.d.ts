
// 假设 TableHandler 和 rewirteFormats 的类型定义如下
interface TableHandler {
  // 在这里添加 TableHandler 的类型定义
}

interface RewriteFormats {
  // 在这里添加 rewirteFormats 的类型定义
}

// 使用 declare module 来扩展 'quill1.3.7-table-module' 的类型声明
declare module 'quill1.3.7-table-module' {
  const TableHandler: TableHandler;
  const rewirteFormats: ()=>void;
  export { TableHandler, rewirteFormats };

  // 如果 'quill1.3.7-table-module' 有一个默认导出，比如 'table'，你可以这样声明它
  const table: any; // 或者根据实际情况提供具体的类型
  export default table;
}

declare module 'react-latex';
declare module 'quill/blots/block';
declare module 'quill/blots/inline';
declare module 'dom-to-image';
declare module 'quill/blots/embed';
declare module 'quill/modules/formula';

import MathfieldElement from 'mathlive/dist/types/mathfield-element';
declare global {
  namespace JSX {
    interface IntrinsicElements {
      'math-field': React.DetailedHTMLProps<React.HTMLAttributes<MathfieldElement>, MathfieldElement>;
    }
  }
}