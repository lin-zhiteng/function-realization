import React from 'react';
import { Outlet } from 'react-router-dom';

export default function ExampleDetail() {
  return <>
    <div className='scrollElement' style={{
      width: '100vw',
      height: '100vh',
      overflow: 'auto',
    }}>
      <Outlet />
    </div>
  </>
}