import { useState, useEffect, useRef, useMemo, useCallback } from 'react';
import WithErrorBoundary from '../../WithErrorBoundary';
import { Card, Footer, FrameSty, SectionListSty, Title } from './style';
import gsap from 'gsap';
import StreamerFont from '@/components/StreamerFont';
import StreamerButton from '@/components/StreamerButton';
import { ScrollTrigger } from 'gsap/all';
import useSkip from '@/hooks/useSkip';
import { SkipType } from '@/enum';
import { useGSAP } from '@gsap/react';
import { openNewTab } from '@/utils/router';

gsap.registerPlugin(ScrollTrigger);

function Home() {
  const { skipTo } = useSkip();

  const animation = useRef<any>(null);

  useGSAP(() => {
    gsap.set('.box', {
      x: i => i * 600,
    });

    animation.current = gsap.to('.box', {
      duration: 15,
      ease: 'none',
      x: `+=${2400}`,
      modifiers: {
        x: gsap.utils.unitize(x => parseFloat(x) % 2400),
      },
      repeat: -1,
    });
  }, []);

  return (
    <FrameSty style={{ overflowX: 'hidden' }}>
      <Title>
        <StreamerFont text="卸任的博客" />
      </Title>

      <SectionListSty
        onMouseOver={() => {
          animation.current.pause();
        }}
        onMouseOut={() => {
          animation.current.resume();
        }}
      >
        <Card className="box">
          <p>功能demo</p>
          <StreamerButton
            color="#ee5100"
            handleClick={() => {
              skipTo(SkipType.FUN);
            }}
          >
            Enter
          </StreamerButton>
        </Card>
        <Card className="box">
          <p>掘金文章</p>
          <StreamerButton
            color="#ee5100"
            handleClick={() => {
              openNewTab('https://juejin.cn/user/1733287136470952/posts');
            }}
          >
            Enter
          </StreamerButton>
        </Card>
        <Card className="box">
          <p>Gitee</p>
          <StreamerButton
            color="#ee5100"
            handleClick={() => {
              openNewTab('https://gitee.com/lin-zhiteng');
            }}
          >
            Enter
          </StreamerButton>
        </Card>
        <Card className="box">
          <p>关于</p>
          <StreamerButton
            color="#ee5100"
            handleClick={() => {
              skipTo(SkipType.ABOUT);
            }}
          >
            Enter
          </StreamerButton>
        </Card>
      </SectionListSty>

      <Footer>
        <a href="https://beian.miit.gov.cn/" target="_bank" rel="nofollow">
          闽ICP备2021013399号-1
        </a>
      </Footer>
    </FrameSty>
  );
}

export default WithErrorBoundary(Home);
