import styled from "styled-components";

export const FrameSty = styled.div`
  position:relative;
  width: 100%;
  min-height:100vh;
  background: radial-gradient(ellipse at center, #232333 0%,#161616 100%);
`

export const Title = styled.div`
  margin-top: 140px;
  text-align:center;
  width:100%;
`

export const SectionListSty = styled.div`
  position:relative;
  transform:translateX(-600px);
  margin-top:100px;

  .box{
    position:absolute;
    left:0;
  }
`
export const Card = styled.div`
  display:flex;
  flex-direction:column;
  justify-content:center;
  align-items:center;
  height:260px;
  width:600px;
  flex-shrink: 0;
  border: 1px solid #cccccc;
  border-radius: 8px;
  box-shadow: 1px 1px 5px 1px #fff;
  box-sizing: border-box;

  p{
    font-size:32px;
    color:#fff;
  }

  a{
    text-decoration:none;
  }
`


export const Footer = styled.div`
  position: absolute;
  width: 100%;
  display: flex;
  justify-content: center;
  bottom: 20px;
  text-align: center;

  a{
     color: #fff;
  }
 
`