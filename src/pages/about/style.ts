import styled from 'styled-components';

export const Frame = styled.div`
  height: 100vh;
  width: 100%;
  background-color:#333;
  padding:120px 32px 0 32px;
`