import React from 'react';
import { Frame } from './style';
import Typewriter from '@/components/Typewriter';

export default function Index() {
  return (
    <Frame>
      <Typewriter
        textList={['姓氏：林', '职位：Web前端工程师', '在职情况：在职', '电子邮箱：linzhiteng2022@163.com']}
      />
    </Frame>
  );
}
