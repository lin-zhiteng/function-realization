import React, { useState } from 'react';
import Cascader from '@/components/Cascader';

export default function Index() {

  const [valueOne, setValueOne] = useState('1-1');
  const [valueTwo, setValueTwo] = useState('1-1-1');
  const [valueThree, setValueThree] = useState('1-1');

  return (
    <>
      <div
        style={{
          height: '100px',
        }}
      ></div>
      <Cascader value={valueOne} handleChange={(id)=>{
        setValueOne(id);
      }} />
      <Cascader value={valueTwo} handleChange={(id)=>{
        setValueTwo(id);
      }}/>
      <Cascader value={valueThree} handleChange={(id)=>{
        setValueThree(id);
      }} />
    </>
  );
}
