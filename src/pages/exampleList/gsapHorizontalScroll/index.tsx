import React, { useRef } from 'react';
import { Section, Title } from './style';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/all';
import { useGSAP } from '@gsap/react';
import img1 from '@/assets/imgs/1.jpg';
import img2 from '@/assets/imgs/2.jpg';
import img3 from '@/assets/imgs/3.jpg';
import img4 from '@/assets/imgs/4.jpg';

gsap.registerPlugin(ScrollTrigger);

export default function Index() {
  const container = useRef(null);
  const box = useRef<HTMLDivElement>(null);

  useGSAP(() => {
    if (box.current === null) {
      return;
    }
    gsap.to(box.current, {
      x: -(box.current.offsetWidth - window.innerWidth),
      scrollTrigger: {
        scroller: container.current,
        trigger: box.current,
        scrub: 2,
        // markers: true,
        end: '+=800',
        pin: true,
      },
    });
  }, []);

  return (
    <div
      ref={container}
      style={{
        height: '100vh',
        overflowY: 'auto',
        overflowX: 'hidden',
      }}
    >
      <Title>
        <h2>Horizontal scroll section</h2>
      </Title>

      <Section ref={box}>
        <p>
          秋天悄然降临，带来了清爽的微风和金黄的落叶。树木在这时节换上了最华丽的衣裳，黄、橙、红交相辉映，犹如一幅醉人的油画。空气中弥漫着丰收的气息，果园里苹果、梨子和葡萄挂满枝头，散发着诱人的香味。
        </p>
        <img src={img1} alt="" />
        <img src={img2} alt="" />
        <img src={img3} alt="" />
        <img src={img4} alt="" />
      </Section>

      <div
        style={{
          height: '100vh',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <h2>End</h2>
      </div>
    </div>
  );
}
