import styled from 'styled-components';

export const Title = styled.div`
  height:100vh;
  display:flex;
  justify-content:center;
  align-items:center;
  font-size:36px;
`

export const Section = styled.section`
  padding:0px 30px;
  display:inline-flex;
  align-items:center;
  gap:20px;
  height:100vh;
  background-color:#333;
  color:#fff;

  img{
    height:300px;
    width:500px;
    object-fit:cover;
  }
`