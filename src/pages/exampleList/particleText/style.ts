import styled from "styled-components";

const FrameStyle = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
  background-color: black;
  overflow: hidden;
`

const CanvasStyle = styled.canvas`
  position: absolute,
  top: 0,
  left: 0,
  zIndex: 2,
`

export {
  FrameStyle,
  CanvasStyle
}