import { useState, useEffect, useRef, useMemo, useCallback } from 'react'
import img1 from "@/assets/imgs/1.jpg"
import WithErrorBoundary from '@/WithErrorBoundary';
import { Button } from 'antd';
import Print from '@/components/Print';

const PrintPage = () => {
  const [isShow, setIsShow] = useState(false);
  const canvasRef = useRef<any>(null)
  const canvasCtx = useRef<any>(null)

  useEffect(() => {
    if (canvasRef.current == null) {
      return
    }
    canvasCtx.current = (canvasRef.current as any).getContext('2d')
    canvasCtx.current.fillStyle = 'yellow';
    canvasCtx.current.fillRect(0, 0, 100, 100)
    canvasCtx.current.fillStyle = 'red';
    canvasCtx.current.fillText("我是canvas", 10, 50);
  }, [])


  return (
    <>
      <Print isShow={isShow} handleCancel={() => setIsShow(false)}  >
        <p>我是普通文字</p>
        <br />
        <img style={{
          height: '200px'
        }} src={img1} />
        <br />
        <div>
          form
          <input type="text" />
          <input type="checkbox" />
          <input type="radio" />
        </div>
        <div>
          <button>我是link按钮样式</button>
        </div>
        <canvas ref={canvasRef}></canvas>
      </Print>
      <Button onClick={() => setIsShow(true)}>打印</Button>
    </>
  )
}

export default WithErrorBoundary(PrintPage)
