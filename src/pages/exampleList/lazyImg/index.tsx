import { useState, useEffect, useRef, useMemo, useCallback } from 'react'
import Img from '@/components/Img'
import img1 from '@/assets/imgs/1.jpg'
import { FrameSty, GapSty } from './style'

export default function Index() {
  return (
    <>
      <GapSty height={200} />
      <FrameSty>
        <GapSty height={800} />
        <div style={{
          width: '200px',
          height: '200px',
        }}>
          <Img src={img1} isLazy={true} />
        </div>
      </FrameSty>
    </>
  )
}