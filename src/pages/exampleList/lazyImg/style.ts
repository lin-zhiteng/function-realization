import styled from "styled-components";

interface GapStyProps { 
  height?: number
}
const GapSty = styled.div((props:GapStyProps) => {
  return {
    height: `${props.height}px` || "20px",
  }
})
const FrameSty = styled.div`
  width: 100%;
  height: 400px;
  overflow:scroll;

  img{
    width: 200px;
    height: 200px;
    object-fit: cover;
  }
`

export {
  FrameSty,
  GapSty
}

