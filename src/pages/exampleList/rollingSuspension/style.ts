import styled from "styled-components";

const TopFrameSty = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: red;
  height: 100vh;
  width: 100%;
`

const CanvasSty = styled.div`
  position: relative;
  top: 0px;
  bottom: 0px;
  height: 100vh;
  width: 100%;
`

const BottomFrameSty = styled.div`
  background-color: blue;
  height: 100vh;
  width: 100%;
`

export {
  TopFrameSty,
  CanvasSty,
  BottomFrameSty
}