import styled from 'styled-components';

export const Title = styled.div`
  display:flex;
  justify-content:center;
  align-items:center;
  height: 100vh;
  font-size:36px;
  color: #333;
  background-color:#fff

`

export const Box = styled.div`
  display:flex;
  justify-content:center;
  align-items:center;
  height: 100vh;
  color: rgba(255, 255, 255, 0.1);
  padding: 0 30px;
  box-sizing: border-box;
`

export const Text = styled.span<{
  distance: number;
}>`
  font-size: 36px;
  line-height: 1.5;
  background-image: 
    //揭示高亮的部分
    linear-gradient(90deg, transparent 0ch, #ffffff 0ch),
    //写在最下面的会在下面
    linear-gradient(90deg, transparent 0%, #7f7f7f 0%); //划过的部分
  background-size: 
    8ch 1lh, //设置大小
    calc(${({ distance }) => distance}px + 8ch) 1lh;
  background-position:
    ${({ distance }) => distance}px 0,
    0px 0;
  background-clip: text;
  background-repeat: no-repeat;
`