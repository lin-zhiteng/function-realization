import React, { useEffect, useMemo, useRef, useState } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/all';
import { useGSAP } from '@gsap/react';
import { Box, Text, Title } from './style';

gsap.registerPlugin(ScrollTrigger);

export default function Index() {
  const ALL_SCROLL_DISTANCE = 1200;
  const container = useRef(null);
  const text = useRef<HTMLDivElement>(null);
  const box = useRef(null);
  const sumLen = useRef(0);
  const [distance, setDistance] = useState(0);

  useGSAP(() => {
    if (text.current === null) {
      return;
    }
    const lineHeight: string = window.getComputedStyle(text.current).lineHeight;
    const textHeight = text.current.offsetHeight;
    const lines = Math.round(textHeight / Number(lineHeight.split('px')[0]));
    sumLen.current = lines * text.current.offsetWidth;

    const tl = gsap.timeline({
      scrollTrigger: {
        scroller: container.current,
        trigger: box.current,
        scrub: 2,
        pin: true,
        end: `+=${ALL_SCROLL_DISTANCE}`,
        onUpdate: self => {
          //前一半的进度
          const progress = self.progress;
          //设置距离
          setDistance((sumLen.current / ALL_SCROLL_DISTANCE) * progress * 100 * (ALL_SCROLL_DISTANCE / 100) * 2);
        },
      },
    });

    tl.to(box.current, {
      duration: ALL_SCROLL_DISTANCE / 2 / 1000, // 持续时间为总滚动距离的一半
      ease: 'none',
    });

    //后一半的动画，将整个文字高亮
    tl.to(box.current, {
      color: '#fff',
      duration: ALL_SCROLL_DISTANCE / 2 / 1000,
      ease: 'none',
    });
  }, []);

  return (
    <div
      ref={container}
      style={{
        height: '100vh',
        overflowY: 'auto',
        overflowX: 'hidden',
        backgroundColor: '#333',
      }}
    >
      <Title>
        <h2>down roll</h2>
      </Title>
      <Box ref={box}>
        <div>
          <Text
            ref={text}
            distance={distance}
            style={{
              wordBreak: 'break-all',
            }}
          >
            The rapid advancement of technology has transformed our daily lives in unprecedented ways. From smartphones
            to artificial intelligence, these innovations have made communication faster, work more efficient, and
            information more accessible.
          </Text>
        </div>
      </Box>

      <Title>
        <h2>End</h2>
      </Title>
    </div>
  );
}
