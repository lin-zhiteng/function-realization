import styled from "styled-components";

const UploadImgFrameSty = styled.div`
  position: relative;
  height: 200px;
  width: 200px;
  margin:20px 0 0 50px ;
  box-sizing: border-box;
  box-shadow: 0 0 10px 0 rgba(0,0,0,0.5);
  border-radius: 10px;
  input{
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1;
    height: 100%;
    width: 100%;
    opacity: 0;
    cursor: pointer;
  }
  img{
    position: absolute;
    z-index: 0;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    object-fit: cover;
    border-radius: 10px;
    cursor: pointer;
  }
`

const IconSty = styled.div`
  position: absolute;
  z-index: 0;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  font-size: 30px;
`


export {
  UploadImgFrameSty,
  IconSty
}