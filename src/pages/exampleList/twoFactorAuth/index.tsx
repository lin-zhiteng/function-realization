import React, { useEffect, useRef, useState } from 'react';
import { authenticator } from '@otplib/preset-browser';
import QRCode from 'qrcode';
import { Button, Upload, UploadFile } from 'antd';
import { Html5QrcodeScanner, Html5QrcodeScanType } from 'html5-qrcode';

authenticator.options = {
  digits: 6, //一次性密码的长度。默认值为 6。
  step: 30, // 有效期限（以秒为单位）。默认值为 30。
  algorithm: 'sha1', //sha1（默认）  sha256  sha512
};

export default function Index() {
  const [url, setUrl] = useState('');

  /** 密钥*/
  const secret = useRef('');

  const handleClick = () => {
    //随机的字符串
    const secret = authenticator.generateSecret();
    //生成一个OTP URI
    const otpauth = authenticator.keyuri('user@example.com', 'My App', secret);
    console.log('otpauth:', otpauth);

    QRCode.toDataURL(otpauth, (err: any, imageUrl: string) => {
      if (err) {
        console.error('Error generating QR code', err);
        return;
      }
      setUrl(imageUrl);
      console.log('QR code image URL:', imageUrl);
    });
  };

  return (
    <>
      <div
        style={{
          height: '200px',
        }}
      ></div>
      <h2>生成二维码</h2>
      <section>
        {url && (
          <img
            style={{
              height: '200px',
              width: '200px',
            }}
            src={url}
            alt="QR code"
          />
        )}
        <div>
          <Button onClick={handleClick}>点击生成二维码</Button>
        </div>
      </section>

      <h2>解析二维码</h2>
      <div id="scanFile"></div>

      <section>
        <div>
          <Button
            onClick={() => {
              let reader = new Html5QrcodeScanner(
                'scanFile',
                {
                  fps: 10,
                  qrbox: { width: 150, height: 150 },
                  supportedScanTypes: [Html5QrcodeScanType.SCAN_TYPE_FILE],
                },
                false
              );

              reader.render(data => {
                const urlObj = new URL(data);
                const label = decodeURIComponent(urlObj.pathname.replace(/^\/+/, ''));
                const params = Object.fromEntries(urlObj.searchParams.entries());
                secret.current = params.secret;
              });
            }}
          >
            上传图片
          </Button>
          <Button
            onClick={() => {
              // 一次性密码
              const otp = authenticator.generate(secret.current);
              console.log(otp, 'otp');
            }}
          >
            获取OTP
          </Button>
        </div>
      </section>
    </>
  );
}
