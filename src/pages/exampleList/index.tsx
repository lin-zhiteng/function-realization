import React, { useCallback, useEffect, useRef } from 'react';
import { FrameSty, ListFrameSty, TitleSty } from './style';
import { routeList } from '@/router';
import { useNavigate } from 'react-router-dom';
import VerticalLetter from '@/components/VerticalLetter';

export default function ExampleList() {
  const navigate = useNavigate();
  const clickItem = useCallback((detail: any) => {
    navigate(`/exampleDetail/${detail.path}`);
  }, []);

  return (
    <FrameSty>
      <TitleSty>功能Demo</TitleSty>

      <ListFrameSty>
        {routeList.map((item, index) => {
          return (
            <button
              className="listItme"
              key={index}
              onClick={() => {
                clickItem(item);
              }}
            >
              <span>{item.title}</span>
            </button>
          );
        })}
      </ListFrameSty>
    </FrameSty>
  );
}
