import React from 'react';
import { Virtuoso } from 'react-virtuoso';

export default function Index() {
  return (
    <>
      <div
        style={{
          height: '100px',
        }}
      ></div>
      <div>
        <Virtuoso
          style={{
            height: '400px',
          }}
          totalCount={200}
          itemContent={index => (
            <div
              style={{
                height: `${30}px`,
              }}
            >
              Item {index}
            </div>
          )}
        />
      </div>
    </>
  );
}
