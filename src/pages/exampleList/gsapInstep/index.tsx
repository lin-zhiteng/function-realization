import React, { useRef, useEffect } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/all';
import { useGSAP } from '@gsap/react';
import img1 from '@/assets/imgs/1.jpg';
import img2 from '@/assets/imgs/2.jpg';
import img3 from '@/assets/imgs/3.jpg';
import img4 from '@/assets/imgs/4.jpg';
import { Card, Section, Title } from './style';

gsap.registerPlugin(ScrollTrigger);

export default function Index() {
  const container = useRef(null);
  const title = useRef(null);
  const img1Ref = useRef(null);
  const p1Ref = useRef(null);
  const img2Ref = useRef(null);
  const p2Ref = useRef(null);
  const img3Ref = useRef(null);
  const p3Ref = useRef(null);
  const img4Ref = useRef(null);
  const p4Ref = useRef(null);

  useGSAP(() => {
    gsap.to(title.current, {
      opacity: 0,
      scrollTrigger: {
        scroller: container.current,
        trigger: title.current,
        scrub: 2,
        // markers: true,
        start: 'top 180', // 动画开始位置
        end: 'bottom top',
      },
    });

    const leftList = [img1Ref.current, img3Ref.current];
    leftList.forEach(element => {
      gsap.from(element, {
        x: -50,
        scrollTrigger: {
          scroller: container.current,
          trigger: element,
          scrub: 1,
          // markers: true,
          start: 'top bottom', // 动画开始位置
          end: 'bottom bottom-=200',
        },
      });
    });

    const rightList = [img2Ref.current, img4Ref.current];

    rightList.forEach(element => {
      gsap.from(element, {
        x: 50,
        scrollTrigger: {
          scroller: container.current,
          trigger: element,
          scrub: 1,
          // markers: true,
          start: 'top bottom', // 动画开始位置
          end: 'bottom bottom-=200',
        },
      });
    });

    const elements = [p1Ref.current, p2Ref.current, p3Ref.current, p4Ref.current];
    elements.forEach(element => {
      gsap.from(element, {
        y: 60,
        opacity: 0,
        scrollTrigger: {
          scroller: container.current, // 如果你有自定义滚动容器
          trigger: element,
          scrub: 1,
          // markers: true,
          start: 'top bottom', // 动画开始位置
          end: 'bottom bottom-=200', // 动画结束位置
        },
      });
    });
  }, []);

  return (
    <div
      ref={container}
      style={{
        height: '100vh',
        overflow: 'auto',
      }}
    >
      <Title>
        <h2 ref={title}>Scroll down and up to see different reveal animations</h2>
      </Title>

      <Section>
        <Card ref={img1Ref}>
          <img src={img1} />
        </Card>

        <div ref={p1Ref}>
          秋天悄然降临，带来了清爽的微风和金黄的落叶。树木在这时节换上了最华丽的衣裳，黄、橙、红交相辉映，犹如一幅醉人的油画。空气中弥漫着丰收的气息，果园里苹果、梨子和葡萄挂满枝头，散发着诱人的香味。人们走在铺满落叶的小路上，脚下发出沙沙的声响，就像奏起了秋天的乐章。
        </div>
      </Section>

      <Section>
        <p ref={p2Ref}>
          秋天悄然降临，带来了清爽的微风和金黄的落叶。树木在这时节换上了最华丽的衣裳，黄、橙、红交相辉映，犹如一幅醉人的油画。空气中弥漫着丰收的气息，果园里苹果、梨子和葡萄挂满枝头，散发着诱人的香味。人们走在铺满落叶的小路上，脚下发出沙沙的声响，就像奏起了秋天的乐章。
        </p>
        <Card ref={img2Ref}>
          <img src={img2} />
        </Card>
      </Section>
      <Section>
        <Card ref={img3Ref}>
          <img src={img3} />
        </Card>

        <p ref={p3Ref}>
          秋天悄然降临，带来了清爽的微风和金黄的落叶。树木在这时节换上了最华丽的衣裳，黄、橙、红交相辉映，犹如一幅醉人的油画。空气中弥漫着丰收的气息，果园里苹果、梨子和葡萄挂满枝头，散发着诱人的香味。人们走在铺满落叶的小路上，脚下发出沙沙的声响，就像奏起了秋天的乐章。
        </p>
      </Section>

      <Section>
        <p ref={p4Ref}>
          秋天悄然降临，带来了清爽的微风和金黄的落叶。树木在这时节换上了最华丽的衣裳，黄、橙、红交相辉映，犹如一幅醉人的油画。空气中弥漫着丰收的气息，果园里苹果、梨子和葡萄挂满枝头，散发着诱人的香味。人们走在铺满落叶的小路上，脚下发出沙沙的声响，就像奏起了秋天的乐章。
        </p>
        <Card ref={img4Ref}>
          <img src={img4} alt="img1" />
        </Card>
      </Section>

      <div
        style={{
          height: '200px',
        }}
      ></div>
    </div>
  );
}
