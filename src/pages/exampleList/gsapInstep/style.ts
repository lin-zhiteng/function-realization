import styled from "styled-components";

export const Title = styled.div`
  padding:0px 100px;
  display:flex;
  justify-content:center;
  align-items:center;
  height:500px;
  font-size:35px;
  text-align:center;
`

export const Section = styled.section`
  padding:0px 30px;
  gap:20px;
  display:flex;
  align-items:center;
`

export const Card = styled.div`
  display:flex;
  justify-content:center;
  align-items:center;
  padding:10px;
  height:260px;
  width:260px;
  flex-shrink: 0;
  border: 1px solid #cccccc;
  border-radius: 8px;
  background: #ffffff;
  box-shadow: 1px 1px 5px 1px #CCCCCC;

  img{
    height:100%;
    width:100%;
    object-fit:cover;
  }
`

