import styled from "styled-components";


export const FrameSty = styled.div`
  .ant-collapse-item >.ant-collapse-header {
    padding: 0px 5px;
  }

  .ant-collapse>.ant-collapse-item:last-child>.ant-collapse-header{
    align-items: center;
    min-height: 32px;
  }

  .ant-collapse-item >.ant-collapse-header >.ant-collapse-expand-icon{
    padding-right: 2px;
  }

  .ant-collapse-item >.ant-collapse-content>.ant-collapse-content-box{
    padding: 0px;
  }

  .list {
    .item{
      margin-bottom: 10px;
    }
    .item:last-child{
      margin-bottom: 0px;
    }
  }


`