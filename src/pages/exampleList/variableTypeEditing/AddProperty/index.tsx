import { Input, Modal } from 'antd'
import React, { useState, useEffect, useRef, useMemo, useCallback } from 'react'

type AddPropertyProps = {
  isOpen: boolean,
  handleOk: (data:string) => void,
  handleCancel: () => void,
}

export default function AddProperty(props: AddPropertyProps) {
  const { isOpen, handleCancel, handleOk } = props
  const [value, setValue] = useState<string>('')

  const onChange = useCallback((e: any) => {
    setValue(e.target.value)
  }, [])

  const onOk = useCallback(() => {
    //检查是否为数字
    const reg = /^\d+$/
    if (reg.test(value)) {
      console.log('属性名不能为数字');
      return
    }

    if (!!value) {
      handleOk(value)
    }
  }, [value])

  useEffect(() => {
    if (isOpen) {
      setValue('')
    }
  }, [isOpen])

  return (
    <>
      <Modal
        title="属性名"
        width='600px'
        open={isOpen}
        onCancel={handleCancel}
        onOk={onOk}
      >
        <Input
          value={value}
          onChange={onChange}
          placeholder="请输入属性名"
        />
      </Modal>
    </>
  )
}