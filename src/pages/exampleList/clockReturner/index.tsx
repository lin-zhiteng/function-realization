import { useEffect, useMemo, useState } from 'react';
import ClockReturner from '@/components/ClockReturner';

function Index() {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        marginTop: '100px',
        alignItems: 'center',
      }}
    >
      <h2
        style={{
          textAlign: 'center',
          marginTop: '100px',
        }}
      >
        当前时间
      </h2>

      <ClockReturner />
    </div>
  );
}

export default Index;
