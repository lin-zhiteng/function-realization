import styled from "styled-components";

const FrameSty = styled.div`
  height: 100%;
  width: 100%;
  overflow: hidden;
`

export { 
  FrameSty
}