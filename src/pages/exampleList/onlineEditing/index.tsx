import React, { useCallback, useEffect, useRef, useState } from 'react';
import OnlineEditing from '@/components/OnlineEditing';
import { Button } from 'antd';

export default function Index() {
  const [alreadyExistType] = useState({
    myObject: {
      name: 'string',
      age: 'number',
    },
    myObjectOne: {
      name: 'string',
      age: 'number',
    },
  });
  const [alreadyExistVar] = useState({
    myObject: {
      name: 'aaaa',
      age: 18,
    },
    myObjectOne: {
      name: 'bbbb',
      age: 18,
    },
  });
  const [isModalOpen, setIsModalOpen] = useState(false);
  const handleOk = (data: any) => {
    const code = data;
    const wrappedCode = `
      var exports = {};
      ${code.replace('export default', 'exports.default =')}
      return exports.default;
    `;
    const myObject = new Function(...Object.keys(alreadyExistVar), wrappedCode)(...Object.values(alreadyExistVar));
    console.log(myObject);
    setIsModalOpen(false);
  };

  return (
    <>
      <div>sad</div>
      <div>sad</div>
      <Button
        type="primary"
        onClick={() => {
          setIsModalOpen(true);
        }}
      >
        打开编辑器
      </Button>
      <OnlineEditing
        alreadyExistType={alreadyExistType}
        isShow={isModalOpen}
        handleOk={handleOk}
        handleCancel={() => {
          setIsModalOpen(false);
        }}
      />
    </>
  );
}
