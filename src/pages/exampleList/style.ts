import styled, { css } from "styled-components";

//创建一个样式片段
export const shadowCss = css` 
  box-shadow: 
  -.5rem -.5rem 1rem hsl(0 0% 100% / .75),
  .5rem .5rem 1rem hsl(0 0% 50% / .5);
`
export const shadowCssOne = css` 
  box-shadow: 
  -.5rem -.5rem 1rem hsl(0 0% 100% / .75),
  .5rem .5rem 1rem hsl(0 0% 50% / .5),
  inset .5rem .5rem 1rem hsl(0 0% 50% / .5),
  inset -.5rem -.5rem 1rem hsl(0 0% 100% / .75);
`

export const FrameSty = styled.div`
  height: 100%;
  min-height: 100vh;
  width: 100vw;
  background-color: #e5e9f4;
  padding:0 4rem 4rem 4rem;
  box-sizing: border-box;
`

export const TitleSty = styled.div`
  display:inline-flex;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top:130px;
  font-size: 46px;
  font-weight: 500;
  font-family: "Ma Shan Zheng", cursive;
`

export const ListFrameSty = styled.div`
  display: grid;
  margin-inline: auto;
  grid-template-columns: repeat(auto-fit, minmax(min(8rem, 100%), 1fr));
  gap: 2rem;
  margin-top:72px;
  
  .listItme {
    container-type: inline-size;
    aspect-ratio: 1/1;
    border: 0.5rem solid transparent;
    border-radius: 1rem;
    color: hsl(0 0% 10%);
    background: none;
    cursor: pointer;
    
    display: grid;
    place-content: center;
    gap: 1rem;
    
    ${shadowCss}
    outline: none;  
    transition: all 0.1s;
    
    &:hover, &:focus-visible {
      color: #3498db;
      scale: 1.1
    }
    &:active, &.active{
      ${shadowCssOne}
      color: #3498db;
      > span { 
        font-size: 13px;
        font-weight: 550
      };
    }

    > span {
      font-family: system-ui, sans-serif;
      font-size: 16px;
      font-weight: 550;
    }
  }

`