import styled from "styled-components";
const FrameSty = styled.div`
  height: 100%;
  width: 100%;
`
const IntervalSty = styled.div`
  height: 500px;
  width: 100%;
`


const EnlargeSty = styled.div`
  height: 260px;
  width:220px;
  margin-left: 60px;
  padding: 10px;
  box-shadow: 0 0 10px 0 rgba(0,0,0,0.5);
  border-radius: 5px;
  box-sizing: border-box;
`
const ImgDivSty = styled.div`
  height: 200px;
  width: 200px;
  img{
    height: 100%;
    width: 100%;
    object-fit: contain;
    border-radius: 5px;
  }
`

const TextSty = styled.div`
  margin-top: 10px;  
  text-align: center;
`


export {
  FrameSty,
  IntervalSty,
  EnlargeSty,
  ImgDivSty,
  TextSty
}