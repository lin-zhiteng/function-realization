import { useState, useEffect, useRef, useMemo, useCallback } from 'react'
import img1 from "@/assets/imgs/1.jpg"
import useMoveScreen from '@/hooks/useEnlargePreview'
import { EnlargeSty, FrameSty, ImgDivSty, IntervalSty, TextSty } from './style'

export default function Index() {
  const { frameDom, domStyle } = useMoveScreen()

  return (
    <FrameSty>
      <IntervalSty />
      <EnlargeSty ref={frameDom}
        style={{
          ...domStyle
        }}
      >
        <ImgDivSty>
          <img src={img1} />
        </ImgDivSty>
        <TextSty>我是一个图片</TextSty>
      </EnlargeSty>
      <div>
        <h3>用来判断放大时页面元素是否发生位移</h3>
      </div>
      <IntervalSty />
    </FrameSty>
  )
}