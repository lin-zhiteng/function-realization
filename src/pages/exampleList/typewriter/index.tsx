import Typewriter from '@/components/Typewriter';
import React, { useState } from 'react';

export default function Index() {
  const [textList, setTextList] = useState([
    'In the quiet embrace of dawn, the world awakens to a symphony of colors',
    'The sun, a gentle giant, peeks over the horizon, casting a warm glow that dances upon the dew-kissed leaves',
    'Birds, in their early morning chorus, sing of hope and new beginnings',
  ]);

  return (
    <div
      style={{
        height: '100vh',
        width: '100%',
        backgroundColor: '#333',
      }}
    >
      <div style={{ height: '200px' }}></div>
      <Typewriter textList={textList} />
    </div>
  );
}
