import styled from "styled-components";


const FrameSty = styled.div`
  display: flex;
  position: relative;
  height: 400px;
  width: 100%;
  margin-top: 50px;
  background-color: #ccc;
  overflow-y:hidden ;
`

const ListSty = styled.div`
  flex: 1;
`

const ListItemSty = styled.div` 
  display: flex;
  img{
    height: 50px;
    width: 50px;
  }
  word-break:break-all;
`

const ListScrollBar = styled.div`
  width: 10px;
  background-color: red;
`

export {
  FrameSty,
  ListSty,
  ListItemSty,
  ListScrollBar
}