import { useState, useEffect, useRef, useMemo, useCallback } from 'react';
import { Button, message } from 'antd';
import { request } from '@/utils/axiox';

export default function Index() {
  const [messageApi, contextHolder] = message.useMessage();

  /** 发送请求*/
  const sendRequest = useCallback(() => {
    request('/test')
      .then(res => {})
      .catch(err => {
        messageApi.open({
          type: 'error',
          content: err.message,
        });
      });
    request('/test')
      .then(res => {})
      .catch(err => {
        messageApi.open({
          type: 'error',
          content: err.message,
        });
      });
    request('/test')
      .then(res => {})
      .catch(err => {
        messageApi.open({
          type: 'error',
          content: err.message,
        });
      });
  }, []);

  /** 清除accessToken和refreshToken*/
  const clearToken = useCallback(() => {
    messageApi.open({
      type: 'success',
      content: '已清除',
    });
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
  }, []);

  /** 登录*/
  const onLogin = useCallback(() => {
    request('/login').then((res: any) => {
      if (res.code === 200) {
        messageApi.open({
          type: 'success',
          content: '登录成功',
        });
        const { accessToken, refreshToken } = res.data.tokenObj;
        localStorage.setItem('accessToken', accessToken);
        localStorage.setItem('refreshToken', refreshToken);
      }
    });
  }, []);

  return (
    <>
      {contextHolder}
      <Button onClick={onLogin}>点击我登录</Button>
      <Button onClick={sendRequest}>发送请求</Button>
      <Button onClick={clearToken}>清空token</Button>
    </>
  );
}
