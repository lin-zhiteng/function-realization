import styled from "styled-components";

const TitleSty = styled.div`
  text-align: center;
`

const ContentSty = styled.section`
  position: relative;
  box-sizing: border-box;
` 

export {
  TitleSty,
  ContentSty
}
