import ReactQuill, { Quill } from 'react-quill';
import { useState, useEffect, useRef, useMemo, useCallback } from 'react';
import 'react-quill/dist/quill.snow.css';
// //数学公式
import 'katex/dist/katex.css';
import RichText from '@/components/RichText';

type Props = {
  value?: any;
  handleChange?: (data: any) => void;
};

export default function Index(props: Props) {
  const [value, setValue] = useState('');

  useEffect(() => {
    setValue(
      '<p>12321<span class="ql-formula" data-value="\\left ( \\frac{a}{b}\\right )^{n}= \\frac{a^{n}}{b^{n}}"></span> 12312231123</p>'
    );
  }, []);

  const getHtmlString = () => {
    let parser = new DOMParser();
    let doc = parser.parseFromString(value, 'text/html');

    // 使用得到的Document对象查找class为"ql-formula"
    let elements = doc.getElementsByClassName('ql-formula');
    Array.from(elements).forEach(element => {
      //去除element的子元素
      while (element.firstChild) {
        element.removeChild(element.firstChild);
      }
    });

    let updatedHtml = doc.body.innerHTML.replace(/(data-value=")([^"]*)"/g, function (match, p1, p2) {
      // 替换data-value中的单个反斜杠为双反斜杠
      return p1 + p2.replace(/\\/g, '\\\\') + '"';
    });
    console.log(updatedHtml);
    return updatedHtml;
  };

  return (
    <>
      <div style={{ height: '100px' }}></div>
      <RichText
        value={value}
        handleChange={(str: any) => {
          setValue(str);
        }}
      />

      <div onClick={getHtmlString}>传给后端的数据</div>
    </>
  );
}
