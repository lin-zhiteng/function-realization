import styled from "styled-components";


export const FrameSty = styled.div`
  width:100%;
  height:100%;
  background-color:#14151a;
  color:#fff;
  padding-top:140px;
  text-align:center;

  .item{
    animation: enter 1.2s ease;
  }

  @keyframes enter {
    0% {
      opacity: 0;
      transform: translateY(100px);
    }
    100% {
      opacity: 1;
      transform: translateY(0);
    }
  }
`

export const TitleFrame = styled.div<{
  opacity: number
}>`
  width:100%;
  opacity:${props => props.opacity};
  text-align:center;

  .title{ 
    font-size: 60px;
    font-weight: 700;
    margin-bottom: 20px;
     font-family: "Kalam", cursive;
  }

  .describe{
    font-size: 32px;
    font-weight: 400;
    margin-top:10px;
    font-family: "Ma Shan Zheng", cursive;
  }
`

export const ArticleItemSty = styled.div`
  margin-top: 80px;
  font-size: 19px;

  .item{
    margin-top:'30px'
  }

`