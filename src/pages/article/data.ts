
export const currenList = [
  {
    title: "使用高阶组件封装路由拦截逻辑",
    url: "https://juejin.cn/post/7416275753702424610"
  },
  {
    title: "React视图和逻辑分离，容器组件和展示组件",
    url: "https://juejin.cn/post/7413629689254674486"
  },
  {
    title: "从0到1搭建React从路由权限到按钮权限",
    url: "https://juejin.cn/post/7413335608464097319"
  },
  {
    title: "GSAP解锁动画的无限可能",
    url: "https://juejin.cn/post/7396932633169723446"
  },
  {
    title: "实现GPT多行打字机效果",
    url: "https://juejin.cn/post/7393644512005799946"
  },
  {
    title: "WebHooks配合Docker实现项目CI/CD",
    url: "https://juejin.cn/post/7392788843097407514"
  },
  {
    title: "Corepack同一管理JS包管理器版本",
    url: "https://juejin.cn/post/7386505099848859702"
  },
  {
    title: "Quill中可视化编辑公式，并在RN中渲染",
    url: "https://juejin.cn/post/7370897221367693363"
  },
  {
    title: "React中如何编写一个全局Toast",
    url: "https://juejin.cn/post/7366813022008918043"
  },
  {
    title: "从0到1搭建基于@tarojs/components组件库",
    url: "https://juejin.cn/post/7353106546827542543"
  },
  {
    title: '别光用模拟器打游戏了，开发也是事半功倍',
    url: 'https://juejin.cn/post/7343432622583201842'
  },
]

export const supplementList = [
  {
    title: '使用computed配合Proxy拦截v-model',
    url: 'https://juejin.cn/post/7338634091397431330'
  },
  {
    title: '什么色彩空间与微信视频黑屏有关！',
    url: 'https://juejin.cn/post/7337946762358177792'
  },
  {
    title: '仿MongoDB写一个数据编辑弹窗',
    url: 'https://juejin.cn/post/7330658031541567540'
  },
  {
    title: 'Token无感知刷新',
    url: 'https://juejin.cn/post/7317106006688448524'
  },
  {
    title: '巩固Error和自定义Error',
    url: 'https://juejin.cn/post/7300758264327700489'
  },
  {
    title: '使用IntersectionObserver实现图片懒加载组件',
    url: 'https://juejin.cn/post/7298989375370788890'
  },
  {
    title: 'WebHooks实现项目CI/CD',
    url: 'https://juejin.cn/post/7298627636392706057'
  },
  {
    title: 'Blob的几个使用场景',
    url: 'https://juejin.cn/post/7293341082687176730'
  },
  {
    title: 'hook实现元素放大浏览',
    url: 'https://juejin.cn/post/7293180747400904754'
  },
  {
    title: '防止遮盖层层下页面滚动，看看Ant Design怎么干的',
    url: 'https://juejin.cn/post/7291152058484162618'
  },
  {
    title: 'React根据文件目录自动生成路由',
    url: 'https://juejin.cn/post/7290157103674130486'
  },
  {
    title: 'React配置路径别名、ESLint设置、git commit前规范检查',
    url: 'https://juejin.cn/post/72886061103365161557'
  },
  {
    title: '实现小红书响应式瀑布流',
    url: 'https://juejin.cn/post/7270160291411886132'
  },
  {
    title: '跟源码手写自己打印预览组件',
    url: 'https://juejin.cn/post/7268539624560492603'
  },
  {
    title: '文字根据宽高自适应省略号',
    url: 'https://juejin.cn/post/7264831413521711123'
  },
  {
    title: 'Canvas动态粒子文字效果',
    url: 'https://juejin.cn/post/7262645115819081784'
  },
  {
    title: 'Canvas实现苹果充电盒动效',
    url: 'https://juejin.cn/post/7260776783394979877'
  },
  {
    title: 'Canvas实现数字雨和放大镜效果',
    url: 'https://juejin.cn/post/7260000504247746620'
  },
  {
    title: '使用TS内置工具类型，减少重复代码',
    url: 'https://juejin.cn/post/7259204885278900284'
  },
  {
    title: '自定义滚动条，手撸不等高虚拟列表',
    url: 'https://juejin.cn/post/7258149598031560759'
  },
  {
    title: '自定义滚动条，手撸等高虚拟列表',
    url: 'https://juejin.cn/post/7256599745425981499'
  },
  {
    title: '从前后端代码看文件下载方式',
    url: 'https://juejin.cn/post/7248909699960422437'
  },
  {
    title: 'NVM管理和切换 Node.js 版本工具的安装和使用',
    url: 'https://juejin.cn/post/7246311040267386940'
  },
  {
    title: '为什么在node环境中总是使用绝对路径',
    url: 'https://juejin.cn/post/7190256586214867000'
  },
  {
    title: 'webpack环境变量在项目中的使用',
    url: 'https://juejin.cn/post/7189238123459510309'
  },
  {
    title: 'verdaccio搭建私有npm服务器',
    url: 'https://juejin.cn/post/7146789549044154376'
  },
  {
    title: 'lerna多包管理',
    url: 'https://juejin.cn/post/7134236574295785479'
  },
  {
    title: '实现请求的并发控制',
    url: 'https://juejin.cn/post/7122747942192021517'
  },
  {
    title: '使用VUE3指令实现图片占位图',
    url: 'https://juejin.cn/post/7111667738971471903'
  },
  {
    title: 'Promise的基本使用',
    url: 'https://juejin.cn/post/7111232475468136462'
  },
  {
    title: 'vue项目hash路由锚点定位失效问题',
    url: 'https://juejin.cn/post/7104892883995459614'
  },
  {
    title: '做一个精美的播放器',
    url: 'https://juejin.cn/post/7099848965843058725'
  },
  {
    title: 'VUE3实现验证码（canvas）',
    url: 'https://juejin.cn/post/7096778016323272711'
  },
  {
    title: '前端利用formData对象上传文件，以链接形式返回',
    url: 'https://juejin.cn/post/7094889199370764295'
  },
  {
    title: '代码实现 环形进度条',
    url: 'https://juejin.cn/post/7093402833625546760'
  },
  {
    title: 'CSS全屏滚动效果',
    url: 'https://juejin.cn/post/7090109367764123655'
  },
  {
    title: '如何拥有一个自己的图标库',
    url: 'https://juejin.cn/post/7086487758088175623'
  },
  {
    title: '前端切片上传文件（可暂停），以链接形式返回',
    url: 'https://juejin.cn/post/7085399558993379359'
  },
  {
    title: '从理论到代码实操，图片预加载和懒加载',
    url: 'https://juejin.cn/post/7084190879140806669'
  },
  {
    title: '从理论到代码实操，浅谈JS单线程',
    url: 'https://juejin.cn/post/7083550652940419108'
  },
  {
    title: '前端面试常见的浏览器缓存（强缓存、协商缓存），代码实操',
    url: 'https://juejin.cn/post/7083178636852854792'
  },
  {
    title: 'JS中对数字数组sort失效问题',
    url: 'https://juejin.cn/post/7083143319164813348'
  },
  {
    title: 'blur事件与click事件冲突的解决办法',
    url: 'https://juejin.cn/post/7082775092425392159'
  },
  {
    title: '解决Div标签中数字和字母溢出问题',
    url: 'https://juejin.cn/post/7082242919914012703'
  },
  {
    title: '简单的解决VUE3打包空白问题',
    url: 'https://juejin.cn/post/7082234093181599780'
  },
  {
    title: '简单的判断是单击还是长按',
    url: 'https://juejin.cn/post/7081979460097933326'
  },
  {
    title: '简单的文件直传阿里OSS',
    url: 'https://juejin.cn/post/7081633269812461582'
  },
  {
    title: 'VUE3实现剪贴板复制功能',
    url: 'https://juejin.cn/post/7081558120731770894'
  },
  {
    title: '使用PicGo构建github图床',
    url: 'https://juejin.cn/post/7081313251346087972'
  },
  {
    title: '开源免费软件Dev-sidecar快速打开github',
    url: 'https://juejin.cn/post/7081288664558338061'
  },
]