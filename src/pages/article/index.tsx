import React, { useCallback, useEffect, useRef, useState } from 'react';
import { currenList, supplementList } from './data';
import { ArticleItemSty, FrameSty, TitleFrame } from './style';
import ButtonStyOne from '@/components/ButtonStyOne';
import TextLine from '@/components/TextLine';

export default function Index() {
  const allHeight = 180;
  const animationRef = useRef<any>();
  const isTrue = useRef(false)
  const observeBlock = useRef<any>(null)
  const currentInd = useRef(0);
  const isCheck = useRef(false);
  const isLoadAll = useRef(false);
  /** 滑动过快检测*/
  const slipClearance = useRef(true);

  /** 剩下的全部数据*/
  const [allList, setAllList] = useState(supplementList);
  /** 渲染的列表*/
  const [renderList, setRenderList] = useState(currenList);
  const [transparency, setTransparency] = useState(1)

  const changeIsTrue = () => {
    isTrue.current = false;
    //防止滑动太快
    if (isCheck.current && !isLoadAll.current) {
      if (slipClearance.current) {
        setRenderList((pre) => {
          let newArr: any = [...currenList, ...allList.slice(0, currentInd.current + 1)]
          return newArr;
        })
        slipClearance.current = false;

        setTimeout(() => {
          slipClearance.current = true;
          currentInd.current++;
          if (currentInd.current >= allList.length) {
            isLoadAll.current = true;
          }
        }, 1400)
      }
    }
    animationRef.current = requestAnimationFrame(changeIsTrue);
  }

  useEffect(() => {

    let observer = new IntersectionObserver((entries, observer) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          if (isLoadAll.current === false) {
            setRenderList((prev) => {
              let result: any = [...currenList, ...allList.slice(0, currentInd.current + 1)]
              return result
            })
            currentInd.current = currentInd.current + 1;
            if (currentInd.current >= allList.length) {
              isLoadAll.current = true
            }
            isCheck.current = true
          }
        } else {
          isCheck.current = false;
        }
      });
    });
    observer.observe(observeBlock.current);
    changeIsTrue()
    //监听滚动
    window.addEventListener('scroll', () => {
      if (isTrue.current) {
        return
      }
      setTransparency(1 - document.documentElement.scrollTop / allHeight)
      isTrue.current = true
    })
    return () => {
      cancelAnimationFrame(animationRef.current)
      observer.disconnect()
    }
  }, [])


  return <FrameSty >
    <TitleFrame opacity={transparency} >
      <div className='title'>ARTICLE</div>
      <div className='describe'>分享知识、技巧、经验和见解</div>
      <div className='describe'>记录历程、个人成长以及变化过程</div>
    </TitleFrame>
    {
      renderList.map((item: any, index) => {
        return <ArticleItemSty key={index}>
          <div
            className='item'
          >
            {item.title}
            <TextLine style={{
              marginTop: '20px'
            }}>
              <ButtonStyOne
                broderColor='#00FFDF'
                handelClick={() => {
                  window.open(item.url)
                }}
              >
                Enter
              </ButtonStyOne>
            </TextLine>
          </div>
        </ArticleItemSty>
      })
    }
    <div ref={observeBlock} style={{
      visibility: 'hidden'
    }}>
      我是检测块
    </div>
  </FrameSty>
}