import { getBrothersAndAncestors, getChildrenData, ResData } from '@/components/Cascader/data';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { RootState } from '.';


export interface Option {
  parentId: string,
  value: string,
  label: string,
  isLeaf: boolean,
  children?: Option[]
}

let isLoading = false;
// 初始化分类
export const initFetch = createAsyncThunk('data/initFetch', async () => {
  if (isLoading) {
    return Promise.reject('loading')
  }
  isLoading = true;
  const response = await getChildrenData()
  let result: Option[] = []

  response.forEach((item) => {
    result.push({
      parentId: '0',
      value: item.id,
      label: item.label,
      isLeaf: item.isLeaf,
    })
  })
  isLoading = false;

  return result;
});

/** 获取分类祖籍*/
export const parentsFetch = createAsyncThunk('data/parentsFetch', async (id: string, thunkAPI) => {
  if (isLoading) {
    return Promise.reject('loading')
  }
  isLoading = true;
  const state = thunkAPI.getState() as RootState;
  const mergeArrays = (arr1: Option[], arr2: ResData[], parentId: string = ''): Option[] => {
    const result: Option[] = [];
    let idSet = new Set();
    let uniqueIds: string[] = []
    arr1.forEach(item => {
      idSet.add(item.value)
    })
    arr2.forEach((item: any) => {
      idSet.add(item.id)
    })
    for (let id of idSet) {
      uniqueIds.push(id as string)
    }

    uniqueIds.forEach(id => {
      const item1 = arr1.find((item) => item.value === id);
      const item2 = arr2.find((item) => item.id === id);

      const mergedItem: Option = {
        parentId: parentId,
        value: id,
        label: item1 ? item1.label : item2!.label,
        children: [],
        isLeaf: item2 ? item2.isLeaf : item1!.isLeaf,
      };

      if (item1 && item1.children) {
        mergedItem.children = mergeArrays(item1.children, item2 ? item2.children || [] : [], id);
      } else if (item2 && item2.children) {
        mergedItem.children = mergeArrays(item1 ? item1.children || [] : [], item2.children, id);
      }

      result.push(mergedItem);
    });

    return result;
  }

  let response = await getBrothersAndAncestors(id)
  let result = mergeArrays(state.tree.treeData, response)
  isLoading = false;
  return result;
})

const initialState: {
  isInit: boolean,
  treeData: Option[],
  loading: boolean
} = {
  isInit: false,
  treeData: [],
  loading: false
}

export const treeSlice = createSlice({
  name: 'treeSlice',
  initialState,
  reducers: {
    changeInit: (state, action) => {
      state.isInit = action.payload
    },
    setTree: (state, action) => {
      state.treeData = action.payload
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(initFetch.pending, (state) => {
        state.loading = true;
      })
      .addCase(initFetch.fulfilled, (state, action) => {
        state.isInit = true;
        state.treeData = action.payload;
        state.loading = false;
      })
      .addCase(parentsFetch.pending, (state) => {
        state.loading = true;
      })
      .addCase(parentsFetch.fulfilled, (state, action) => {
        state.treeData = action.payload;
        state.loading = false;
      })
  }
})

export const { changeInit, setTree } = treeSlice.actions

export default treeSlice.reducer

