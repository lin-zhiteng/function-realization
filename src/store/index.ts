import { configureStore } from '@reduxjs/toolkit'
import treeReducer from './treeSlice'

const store = configureStore({
  reducer: {
    tree: treeReducer,
  },
})

//固定写法
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export default store