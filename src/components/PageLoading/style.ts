import styled from "styled-components";
import  backgroundImgOne from './../../assets/imgs/oSHLAzp.png'
import backgroundImgTwo from './../../assets/imgs/u0BC2ZR.png'

export const FrameSty = styled.div`
  position: fixed;
  z-index: 999;
  width: 100vw;
  height: 100vh;
  background-color: #fff;

  @keyframes spinner {
    100% {
      transform: rotate(3600deg);
    }
  }
  
  .loading-container {
    margin: 0 auto;
    text-align: center;
    position: relative;
    top: 50vh;
    transform: translateY(-50%);
  }
  
  .spinner {
    display: inline-block;
    width: 100px;
    height: 100px;
    background: url(${backgroundImgOne}) center center;
    background-size: contain;
    transition-origin: 50% 50%;
    animation: spinner 3s infinite alternate ease-in-out;
  }
  .spinner-center {
    display: inline-block;
    position: absolute;
    margin-left: -100px;
    width: 100px;
    height: 100px;
    background: url(${backgroundImgTwo}) center center;
    background-size: contain;
    content: '';
  }
  
  .loading-text {
    position: relative;
    z-index: 1;
    font-size: 1.5rem;
    font-family: "Comic Sans MS", cursive, sans-serif;
    margin-left: 0.5em;
  }
`