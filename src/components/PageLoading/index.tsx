import React from 'react'
import { FrameSty } from './style'

export default function PageLoading() {
  return <FrameSty>
    <div className="loading-container">
      <div className="spinner"></div>
      <div className="spinner-center"></div>
      <div className="loading-text">Loading...</div>
    </div>
  </FrameSty>
}