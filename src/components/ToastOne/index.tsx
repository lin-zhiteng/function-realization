import React, { createContext, useCallback, useContext, useState } from 'react';
import { Button, message } from 'antd';

const ToastContext = createContext<any>(null);

export const ToastProvider = ({ children }: any) => {
  const [messageApi, contextHolder] = message.useMessage();

  const show = useCallback((msg: string) => {
    messageApi.info(msg);
  }, [messageApi]);

  return (
    <ToastContext.Provider value={{ show }}>
      {children}
      {contextHolder}
    </ToastContext.Provider>
  );
};

export const useToast = () => {
  const context = useContext(ToastContext);
  return context;
};