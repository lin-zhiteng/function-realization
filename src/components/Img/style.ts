import styled from "styled-components";

type ImgFrameStyProps = {
  objectfit?: "cover" | "contain" | "fill" | "none" | "scale-down" 
}

const ImgFrameSty = styled.div<ImgFrameStyProps>`
  display: inline-block;
  height: 100%;
  width: 100%;

  img{
    height: 100%;
    width: 100%;
    object-fit:${props=>props.objectfit};
  }
`

export {
  ImgFrameSty
}