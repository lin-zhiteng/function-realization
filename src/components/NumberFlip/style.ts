import styled from "styled-components";

export const Frame = styled.div`
  display:inline-block;
  position:relative;
  height:100px;
  width:50px;
  transform-style: preserve-3d;
  perspective:200px;
  border-radius:5px;
  background-color:#333;

  &::before{
    content:"";
    position:absolute;
    top:50%;
    left:0;
    width:100%;
    height:1px;
    background:#666;
    z-index:20;
    transform:translateZ(5px);
  }
`

export const Card = styled.div`
  position:absolute;
  top:0;
  left:0;
  display:flex;
  z-index:10;
  justify-content:center;
  align-items:center;
  height:100px;
  width:50px;
  background:#333;
  color:#fff;
  line-height:100px;
  font-size:38px;
  transform:translateZ(3px);
  border-radius:5px;
  //元素的背面在 3D 变换中是否可见。
  backface-visibility:hidden;
`