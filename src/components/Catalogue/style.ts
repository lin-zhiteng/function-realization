import styled from "styled-components";


const FrameSty = styled.div`
  // position: relative;
  height: 100%;
  overflow-y:scroll;
  overflow-x:hidden;

  &::-webkit-scrollbar {
    width: 6px;
    height: 6px;
  }
  /* 滑块 */
  &:hover::-webkit-scrollbar-thumb {
    background: rgba(0, 0, 0, 0.2);
  }
  /* 滚动槽 */
  &::-webkit-scrollbar-track {
    border-radius: 10px;
  }
  /* 滚动条滑块 */
  &::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background: transparent;
  }
`

const IconSty = styled.div`
  position: absolute;
  z-index: 2;
  right: 6px;
  top: 50%;
  cursor: pointer;
  transform: translate(50%,-50%);
`

export {
  IconSty,
  FrameSty
}