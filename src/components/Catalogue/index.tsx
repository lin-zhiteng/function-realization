import { Button, Menu, MenuProps } from 'antd';
import MenuItem from 'antd/es/menu/MenuItem';
import { useState, useEffect, useRef, useMemo, useCallback } from 'react'
import { useNavigate,useLocation } from 'react-router-dom';
import { LeftCircleOutlined, LeftOutlined, RightCircleOutlined, RightOutlined } from '@ant-design/icons'
import { FrameSty, IconSty } from './style';


type MenuItem = Required<MenuProps>['items'][number];
interface CatalogueProps {
  routeList: any[],
  collapsed?: boolean
}
export default function Catalogue(props: CatalogueProps) {
  const { routeList, collapsed = false } = props
  const location = useLocation();

  console.log([location.pathname.split('/')[1]],'asdasdsa');
  
  const [collapsedState, setCollapsedState] = useState(collapsed)
  const [isShow, setIsShow] = useState(true)
  const iconStyle = useMemo(() => {
    return {
      fontSize: '1.2em',
      color: 'rgba(5, 5, 5, 0.35)',
    }
  }, [])

  const selectedKeys = useMemo(() => {
    let resultData = location.pathname.split('/')[1]
    if(resultData === '') {
      resultData = 'home'
    }
    return [resultData]
  }, [])

  let navigate = useNavigate();

  function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: 'group',
  ): MenuItem {
    return {
      key,
      icon,
      children,
      label,
      type,
    } as MenuItem;
  }

  const menuItem: any = useMemo(() => {
    let resultData = [];
    resultData = routeList.map((item) => {
      return getItem(item.title || '请配置标题', item.path)
    })    
    return resultData
  }, [routeList])

  const onSelect = useCallback((data: any) => {
    navigate(data.key)
  }, [navigate])


  const changeCollapsed = useCallback(() => {
    setCollapsedState(!collapsedState)
    setIsShow(false)
  }, [collapsedState])

  useEffect(() => {
    if (!isShow) {
      setTimeout(() => {
        setIsShow(true)
      }, 400)
    }
  }, [isShow])

  return (
    <FrameSty>
      <Menu
        style={{
          height: '100%',
        }}
        selectedKeys={selectedKeys}
        defaultSelectedKeys={[menuItem[0].key]}
        mode="inline"
        inlineCollapsed={collapsedState}
        items={menuItem}
        onSelect={onSelect}
      />
      <IconSty
        onClick={changeCollapsed}
      >
        {
          isShow && <Button
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: "center"
            }}

            shape="circle"
            icon={collapsedState ?
              <RightOutlined style={iconStyle}  />
              : <LeftOutlined style={iconStyle}  />
            }
          />
        }
      </IconSty>
    </FrameSty>
  )
}