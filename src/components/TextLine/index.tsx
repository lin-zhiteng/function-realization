import React from 'react';
import { FrameSty } from './style';

type Props = {
  children: React.ReactNode,
  style?: React.CSSProperties
}
export default function Index(props: Props) {
  const { children, style } = props;

  return <FrameSty style={style}>
    <div className='line'></div>
    <div className='text'> {children}</div>
    <div className='line'></div>
  </FrameSty>
}