import styled from "styled-components";

export const FrameSty = styled.div`
  display:flex;
  justify-content: center;
  align-items: center;
  .line{
    height:1px;
    flex:1;
    background-color:#333;
  }

  .text{
    padding: 0 10px;
  }
`