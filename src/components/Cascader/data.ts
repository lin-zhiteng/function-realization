let allData = [
  {
    id: '1',
    label: '1',
    isLeaf: false,
    children: [
      {
        id: '1-1',
        label: '1-1',
        isLeaf: false,
        children: [
          {
            id: '1-1-1',
            label: '1-1-1',
            isLeaf: true
          },
          {
            id: '1-1-2',
            label: '1-1-2',
            isLeaf: true
          }
        ]
      },
    ]
  },
  {
    id: '2',
    label: '2',
    isLeaf: true
  }
]

export  interface ResData{
  id: string,
  label: string,
  isLeaf: boolean,
  children?: ResData[]
}

/** 获取某个节点的子节点数据 */
export const getChildrenData = (parentId: string = '0'): Promise<{
  id: string
  label: string
  isLeaf: boolean
}[]> => {
  console.log('触发请求');
  const config: any = {
    '0': [
      {
        id: '1',
        label: '1',
        isLeaf: false,
      },
      {
        id: '2',
        label: '2',
        isLeaf: true
      }
    ],
    '1': [
      {
        id: '1-1',
        label: '1-1',
        isLeaf: false,
      },
    ],
    '1-1': [
      {
        id: '1-1-1',
        label: '1-1-1',
        isLeaf: true
      },
      {
        id: '1-1-2',
        label: '1-1-2',
        isLeaf: true
      }
    ]
  }
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(config[parentId])
    }, 100)
  })
}

/** 获取当前节点的兄弟节点及祖先节点*/
export const getBrothersAndAncestors = (id: string):Promise<ResData[]> => {
  
  console.log('请求兄弟节点及祖先节点');
  
  const config: any = {
    '1-1-1': [
      {
        id: '1',
        label: '1',
        isLeaf: false,
        children: [
          {
            id: '1-1',
            label: '1-1',
            isLeaf: false,
            children: [
              {
                id: '1-1-1',
                label: '1-1-1',
                isLeaf: true
              },
              {
                id: '1-1-2',
                label: '1-1-2',
                isLeaf: true
              }
            ]
          },
        ]
      },
      {
        id: '2',
        label: '2',
        isLeaf: true
      }
    ],
    '1-1': [
      {
        id: '1',
        label: '1',
        isLeaf: false,
        children: [
          {
            id: '1-1',
            label: '1-1',
            isLeaf: false,
          },
        ]
      },
      {
        id: '2',
        label: '2',
        isLeaf: true
      }
    ]
  }

  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(config[id])
    }, 100)
  })

}
