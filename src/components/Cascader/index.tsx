//级联选择器
import { Cascader, CascaderProps } from 'antd';
import React, { useEffect, useState } from 'react';
import useAction from './useAction';

export interface Option {
  value: string;
  label: React.ReactNode;
  children?: Option[];
  isLeaf?: boolean;
}

interface Props {
  value?: string;
  handleChange?: (value: string) => void;
}

export default function Index(props: Props) {
  const { treeData, isInit, loading, isQueryChild, checkIdExists, getChildData } = useAction();
  const { value, handleChange = () => {} } = props;
  const [selfValue, setSelfValue] = useState<string[]>([]);

  const onChange: CascaderProps<Option>['onChange'] = (value, selectedOptions) => {
    if (!!value) {
      handleChange(value[value.length - 1] as string);
      setSelfValue(value as string[]);
    } else {
      handleChange('');
      setSelfValue([]);
    }
  };

  const loadData = (selectedOptions: Option[]) => {
    const targetOption = selectedOptions[selectedOptions.length - 1];
    if (!isQueryChild(targetOption.value)) {
      getChildData(targetOption.value);
    }
  };

  //当初始化完成，并且value有值时，获取子节点数据
  useEffect(() => {
    if (!!value && isInit && selfValue[selfValue.length - 1] != value && !loading) {
      checkIdExists(`${value}`).then((data: string[]) => {
        setSelfValue(data);
      });
    }
  }, [value, isInit, loading]);

  return <Cascader value={selfValue} options={treeData} loadData={loadData} onChange={onChange} changeOnSelect />;
}
