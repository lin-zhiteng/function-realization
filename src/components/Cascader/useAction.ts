import { useAppDispatch, useAppSelector } from '@/hooks/useRedux';
import { initFetch, parentsFetch, setTree } from '@/store/treeSlice';
import React, { useCallback, useEffect } from 'react';
import { Option } from '@/store/treeSlice'
import _ from 'lodash';
import { getChildrenData } from './data';

export default function Index() {

  let isInit = useAppSelector(state => state.tree.isInit)
  let loading = useAppSelector(state => state.tree.loading)
  let treeData = useAppSelector(state => state.tree.treeData)
  const dispatch = useAppDispatch()

  /** 
   * @description 查找分类
   * @param {Option[]} list 需要查找的分类列表
   * @param {string} findId 要查找的分类id
   * @returns {Object} 包含data和listId的对象
   * @property {Option} data 找到的分类
   * @property {string[]} listId 找到的分类的id列表
  */
  const findItem = (list: Option[], findId: string) => {
    let result: Option = null as unknown as Option;
    let listId: string[] = [];

    const fn = (tempList: Option[], findId: string, tempListId: string[]) => {
      for (let i = 0; i < tempList.length; i++) {
        let item = tempList[i]
        if (item.value === findId) {
          listId = [...tempListId, item.value]
          result = item;
          break;
        }
      }

      for (let i = 0; i < tempList.length; i++) {
        let item = tempList[i]
        if (item.children && item.children.length > 0 && result === null) {
          fn(item.children, findId, [...tempListId, item.value])
        }
      }
    }

    fn(list, findId, listId)
    return {
      data: result,
      listId: listId
    }
  }

  /** 
 * @description 当前节点的是否查询过子节点
 * @param findId 要查找的id
 * @returns boolean 是否有查询过
*/
  const isQueryChild = useCallback((parentId: string) => {
    let parent = findItem(treeData, parentId).data
    if (!parent.isLeaf && !!parent.children && parent.children.length > 0) {
      return true
    }
    return false
  }, [treeData])


  const checkIdExists = useCallback((id: string): Promise<string[]> => {
    return new Promise((resolve, reject) => {
      let { data, listId } = findItem(treeData, id)
      if (data === null) {
        dispatch(parentsFetch(id)).then((data) => {
          if (data.type.toString() === parentsFetch.fulfilled.toString()) {
            resolve(findItem(data.payload as Option[], id).listId)
          }
        }).catch(() => {
        })
      } else {
        resolve(listId)
      }
    })
  }, [treeData])

  /** 获取子数据*/
  const getChildData = useCallback((parentId: string) => {
    let tempList = _.cloneDeep(treeData)
    let result = findItem(tempList, parentId).data
    return getChildrenData(parentId).then(data => {
      let list = data.map(item => {
        return {
          parentId: parentId,
          value: item.id,
          label: item.label,
          isLeaf: item.isLeaf,
          children: [],
        };
      });
      result.children = list;
      dispatch(setTree(tempList))
    })
  }, [treeData])

  useEffect(() => {
    if (isInit === false) {
      dispatch(initFetch())
    }
  }, [])

  return {
    treeData,
    isInit,
    loading,
    isQueryChild,
    checkIdExists,
    getChildData
  }
}