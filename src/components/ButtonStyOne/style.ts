import styled from "styled-components";


export const FrameSty = styled.div<{
  $broderColor?: string;
}>`
  display:inline-block;
  cursor:pointer;
	position:relative;
	padding:5px 30px;
	background:white;
	font-size:23px;
	border-top-right-radius:10px;
	border-bottom-left-radius:10px;
	transition:all 1s;
  color:black;
	&:after,&:before{
		content:" ";
		width:10px;
		height:10px;
		position:absolute;
		border :0px solid #fff;
		transition:all 1s;
		}
	&:after{
		top:-1px;
		left:-1px;
		border-top:5px solid ${props => props.$broderColor};
		border-left:5px solid ${props => props.$broderColor};
	}
	&:before{
		bottom:-1px;
		right:-1px;
		border-bottom:5px solid ${props => props.$broderColor};
		border-right:5px solid ${props => props.$broderColor};
	}
	&:hover{
		border-top-right-radius:0px;
	  border-bottom-left-radius:0px;
		&:before,&:after{
			
			width:100%;
			height:100%;
		}
	}

`