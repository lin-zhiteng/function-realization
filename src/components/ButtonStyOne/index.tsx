import React from 'react';
import { FrameSty } from './style';

type Props = {
  children: React.ReactNode,
  handelClick?: () => void;
  broderColor?: string;
}
export default function Index(props: Props) {

  const { children, handelClick, broderColor = 'black' } = props;
  return <FrameSty onClick={handelClick} $broderColor={broderColor}>
    {children}
  </FrameSty>
}