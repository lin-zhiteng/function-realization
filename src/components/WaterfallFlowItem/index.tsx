import { useState, useEffect, useRef, useMemo, useCallback } from 'react'
import { FrameSty, ImgSty, NameSty } from './style'
import Img from '@/components/Img'


export interface WaterfallFlowItemProps {
  /** 显示边界*/
  showBorder: number
  src: string,
  title: string,
  style: React.CSSProperties,
  unitWidth: number,
  index: number,
  sizeChange?: (height: number, index: number) => void
}

export default function Index(props: WaterfallFlowItemProps) {
  let { src, title, style = {}, sizeChange = () => { }, unitWidth, index, showBorder } = props
  let frameDom = useRef(null)
  let [isLoading, setIsLoading] = useState(false)
  let [imgInfo, setImgInfo] = useState<{
    height: number,
    width: number
  }>({
    height: 1,
    width: 1
  })
  /** 是否初始化完成*/
  let isInitAccomplish = useRef(false)

  const imgHeight = useMemo(() => {
    return imgInfo.height * (unitWidth / imgInfo.width);
  }, [imgInfo, unitWidth])

  const finishedLoading = useCallback((imgDom: any) => {
    setImgInfo({
      height: imgDom.naturalHeight,
      width: imgDom.naturalWidth,
    })
    setIsLoading(true)
    isInitAccomplish.current = true
  }, [])

  useEffect(() => {
    isInitAccomplish.current && sizeChange(imgHeight + 40, index)
  }, [imgInfo])

  return (
    <FrameSty style={{
      ...style,
    }} ref={frameDom}>
      <ImgSty>
        {
          <Img
            isLazy={true}
            src={src}
            style={{
              visibility: isLoading ? 'visible' : 'hidden'
            }}
            finishedLoading={finishedLoading}
          />
        }
      </ImgSty>
      <NameSty>
        {title && title}
      </NameSty>
    </FrameSty>
  )
}