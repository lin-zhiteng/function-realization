import React, { useMemo } from 'react';
import { FrameSty } from './style';


type Props = {
  height?: string;
  text: string;
  color?: string;
  activeColor?: string;
  handelClick?: () => void;
  fontSize?:string;
  fontWeight?:number;
}

export default function Index(props: Props) {
  const { height, text, color='#fff', handelClick = () => { },
   activeColor,fontSize='19px',fontWeight=500 } = props;

  const style = useMemo((): React.CSSProperties => {
    return {
      height: height || '100%',
      color: props.color || '#fff'
    }
  }, [height, color])

  const resultActiveColor = useMemo(() => {
    if (activeColor) {
      return activeColor
    }
    return color
  }, [activeColor, color])


  return <>
    <FrameSty 
    style={style} 
    color={color} 
    $resultActiveColor={resultActiveColor}
    fontSize={fontSize}
    fontWeight={fontWeight}
    onClick={handelClick}>
      {text}
    </FrameSty>
  </>
}