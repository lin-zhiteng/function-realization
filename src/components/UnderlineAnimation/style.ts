import styled from "styled-components";

export const FrameSty = styled.div<{
  color?: string;
  $resultActiveColor?: string;
  fontSize?: string;
  fontWeight?: number;
}>`
  text-transform: uppercase;
  text-decoration: none;
  letter-spacing: 0.15em;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  padding: 0 5px;
  position: relative;
  cursor: pointer;
  font-size: ${props => props.fontSize};
  font-weight:${props => props.fontWeight};

  &:after {    
    background: none repeat scroll 0 0 transparent;
    bottom: 0;
    content: "";
    display: block;
    height: 2px;
    left: 50%;
    position: absolute;
    background: ${props => props.color};
    transition: width 0.3s ease 0s, left 0.3s ease 0s;
    width: 0;
  }
  &:hover{
    color: ${props => props.$resultActiveColor} !important;
  }
  &:hover:after { 
    width: 100%; 
    background: ${props => props.$resultActiveColor};
    left: 0; 
  }

`