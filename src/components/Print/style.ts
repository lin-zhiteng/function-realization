import styled from "styled-components";

const FrameContentSty = styled.div`
  position:absolute;
  display: flex;
  flex-direction: column;
  top: 50%;
  left: 50%;
  height: 80%;
  width: 70%;
  transform: translate(-50%, -50%);
  background-color: white;
  border-radius: 5px;
`
const TitleSty = styled.div`
  font-size: 19px;
  text-align: center;
  padding: 10px 0px;
`

const PrintResultSty = styled.div`
  flex: 1;
  width: 100%;
  overflow: auto;
`

const PrintButtonSty = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0px 0px 24px 0px;

  button {
    &:nth-of-type(2) {
      margin-left: 10px;
    }
  }
`

export {
  FrameContentSty,
  TitleSty,
  PrintResultSty,
  PrintButtonSty
}