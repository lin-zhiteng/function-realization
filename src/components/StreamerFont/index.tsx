import React from 'react';
import { FrameSty } from './style';

type Props = {
  text: string;
  fontSize?: string;
  color?: string;
};

export default function Index(props: Props) {
  const { text, fontSize = '5rem', color = '#fff' } = props;
  return (
    <FrameSty fontSize={fontSize} style={{ color }}>
      <p>{text}</p>
    </FrameSty>
  );
}
