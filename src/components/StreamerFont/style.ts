import styled from "styled-components";

export const FrameSty = styled.div<{
  fontSize?: string;
}>`
  p {
    font-size: ${props => props.fontSize};

    padding: 0.25em 0 0.325em;
    display: block;
    margin: 0 auto;
    text-shadow: 0 0 80px rgba(255, 255, 255, 0.5);
    font-family: "Kalam", cursive;
  }

  @-webkit-keyframes aitf {
    0% {
      background-position: 0% 50%;
    }
    100% {
      background-position: 100% 50%;
    }
  }
`