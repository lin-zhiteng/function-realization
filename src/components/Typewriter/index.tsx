// 多行打字机效果
import React, { useEffect, useRef, useState } from 'react';
import { Frame, Item } from './style';

interface Props {
  /** 文字列表 */
  textList: string[];
  /** 每段文字停顿时间 */
  interval?: number;
  /** 每个字符停顿时间 */
  charInterval?: number;
}

export default function Index(props: Props) {
  const { interval = 300, charInterval = 100 } = props;

  const { textList } = props;
  const [currentTextInd, setCurrentTextInd] = useState(0);
  const isInit = useRef(true);
  const textRef = useRef<HTMLDivElement>(null);

  const onTypewriter = () => {
    let len = textRef.current?.children[currentTextInd].innerHTML.length || 0;
    console.log(textRef.current!.children,'len'); 
    textRef.current!.children[currentTextInd].innerHTML = textList[currentTextInd].slice(0, len + 1);
    if (len + 1 === textList[currentTextInd].length) {
      setCurrentTextInd(currentTextInd + 1);
      return;
    }

    setTimeout(() => {
      onTypewriter();
    }, charInterval);
  };

  useEffect(() => {
    if (isInit.current || currentTextInd >= textList.length) {
      return;
    }
    setTimeout(() => {
      onTypewriter();
    }, interval);
  }, [currentTextInd]);

  useEffect(() => {
    isInit.current = false;
  }, []);

  return (
    <>
      <Frame ref={textRef}>
        {textList
          .filter((_, index) => {
            return index <= currentTextInd;
          })
          .map((text, indexOne) => {
            return (
              <Item
                key={indexOne}
                $hideCursor={
                  (currentTextInd != textList.length && currentTextInd != indexOne) ||
                  (currentTextInd == textList.length && indexOne < textList.length - 1)
                }
              />
            );
          })}
      </Frame>
    </>
  );
}
