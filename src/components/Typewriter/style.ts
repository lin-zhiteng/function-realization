import styled from 'styled-components';

export const Frame = styled.div`
   font-family: 'Source Code Pro', monospace;
   color:white;
   font-size:30px;
`

export const Item = styled.div<{
  $hideCursor: boolean;
}>`
  word-break: break-all;

  &::after{
    content: '';
    width: ${props => props.$hideCursor ? '0px' : '3px'} ;
    height: 24px;
    display: inline-block;
    margin-left: 3px;
    background-color: white;
    animation-name: ${props => props.$hideCursor ? 'null' : 'blink'};
    animation-duration: 0.5s;
    animation-iteration-count: infinite;

    @keyframes blink {
      0% {
      opacity: 1;
      }
      50% {
        opacity: 0;
      }
      100% {
        opacity: 1;
      }
    }
  }
`

