import React, { createRef, forwardRef, useImperativeHandle } from 'react';
import { Button, message } from 'antd';

const Toast = forwardRef((props, ref) => {
  const [messageApi, contextHolder] = message.useMessage();

  useImperativeHandle(ref, () => ({
    show: (msg: string) => {
      messageApi.info(msg);
    }
  }));

  return <>
    {contextHolder}
  </>
})

const ToastRef = createRef<{ show: (msg: string) => {} }>();

export const ToastContain = () => {
  return <Toast ref={ToastRef} />
}

export const showToast = (msg: string) => {
  if (ToastRef.current) {
    ToastRef.current.show(msg)
  }
};