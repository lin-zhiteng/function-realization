import styled from "styled-components";

export const QuillFrame = styled.div<{
  $isShowToolbar: boolean
}>`
  .ql-toolbar{
    display: ${props => props.$isShowToolbar ? 'block' : 'none'};
    height: ${props => props.$isShowToolbar ? '100%' : '0'};
  }
  .ql-toolbar.ql-snow + .ql-container.ql-snow {
    border-top: ${props => props.$isShowToolbar ? '0px' : '1px solid #ccc'} ;
  }


  .ql-formula{
    cursor: pointer !important;
    &:hover{
      background-color: #f0f0f0;
    }
  }
`
