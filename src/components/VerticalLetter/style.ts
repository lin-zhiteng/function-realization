import styled from "styled-components";


export const FrameSty = styled.div<{
  color: string,
  $shadowColor: string
}>` 
  display:inline-block;
  @font-face {
    src: url("https://www.axis-praxis.org/fonts/webfonts/MetaVariableDemo-Set.woff2")
      format("woff2");
    font-family: "Meta";
    font-style: normal;
    font-weight: normal;
  }

  transition: all 0.5s;
  -webkit-text-stroke: 4px ${props => props.color};
  font-variation-settings: "wght" 700, "ital" 1;
  letter-spacing:5px;

  font-size: 65px;
  text-align: center;
  color: transparent;
  font-family: "Meta", sans-serif;
  text-shadow: 6px 6px 0px ${props => props.$shadowColor} ;
  cursor: default;
  
  &:hover {
    font-variation-settings: "wght" 100, "ital" 0;
    text-shadow: none;

  }
`