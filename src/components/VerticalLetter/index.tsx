import React, { useMemo } from 'react';
import { FrameSty } from './style';
import { darkenColor } from '@/utils/color';


type Props = {
  text: string;
  color?: string;
  shadowColor?: string;
}

export default function Index(props: Props) {
  const { text, color = '#4fc1ff', shadowColor = '#3498db' } = props;

  return <FrameSty color={color}
    $shadowColor={shadowColor}
  >{text}
  </FrameSty>
}