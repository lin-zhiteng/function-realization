import React from 'react';
import { useEffect, useMemo, useState } from 'react';
import dayjs from 'dayjs';
import NumberFlip, { NumItem } from '@/components/NumberFlip';
import { Colon, Frame, Group } from './style';
import colonSvg from '../../assets/imgs/colon.svg';

export default function Index() {
  const [time, setTime] = useState(dayjs().format('YYYY-MM-DD HH:mm:ss'));

  /** 时十位*/
  const hourDecade = useMemo(() => {
    return Math.floor(dayjs().get('hour') / 10) as NumItem;
  }, [time]);

  /** 时*/
  const hourUnit = useMemo(() => {
    return (dayjs().get('hour') % 10) as NumItem;
  }, [time]);

  /** 分十位*/
  const minuteDecade = useMemo(() => {
    return Math.floor(dayjs().get('minute') / 10) as NumItem;
  }, [time]);

  /** 分*/
  const minuteUnit = useMemo(() => {
    return (dayjs().get('minute') % 10) as NumItem;
  }, [time]);

  /** 秒十位*/
  const secDecade = useMemo(() => {
    return Math.floor(dayjs().get('second') / 10) as NumItem;
  }, [time]);

  /** 秒*/
  const secUnit = useMemo(() => {
    return (dayjs().get('second') % 10) as NumItem;
  }, [time]);

  useEffect(() => {
    setInterval(() => {
      setTime(dayjs().format('YYYY-MM-DD HH:mm:ss'));
    }, 1000);
  }, []);

  return (
    <Frame>
      <Group>
        <NumberFlip value={hourDecade} />
        <NumberFlip value={hourUnit} />
      </Group>

      <Colon>
        <img src={colonSvg} />
      </Colon>

      <Group>
        <NumberFlip value={minuteDecade} />
        <NumberFlip value={minuteUnit} />
      </Group>

      <Colon>
        <img src={colonSvg} />
      </Colon>

      <Group>
        <NumberFlip value={secDecade} />
        <NumberFlip value={secUnit} />
      </Group>
    </Frame>
  );
}
