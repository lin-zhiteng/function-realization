import styled from "styled-components";

export const Frame = styled.div`
  display: flex;
  gap: 15px;
  justify-content: center;
  align-items: center;
`
export const Group = styled.div`
  display: flex;
  gap: 5px;
  justify-content: center;  
  align-items: center;
`

export const Colon= styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`