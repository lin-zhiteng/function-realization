import React from 'react';
import { FrameSty } from './style';

type Props = {
  handleClick?: () => void;
  color?: string;
  children?: React.ReactNode;
}

export default function Index(props: Props) {
  const { handleClick, color = '#03e9f4' } = props;

  return <FrameSty color={color}>
    <button className='button' onClick={handleClick}>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      {props.children}
    </button>
  </FrameSty>
}