import styled from 'styled-components';
const AppFrameSty = styled.section`
  position: relative;
  display: flex;
  height: 100%;
  width: 100%;
`

export const HeaderSty = styled.div`
  display:flex;
  gap: 40px;
  justify-content: center;
  align-items: center;
  position:fixed;
  height: 60px;
  width: 100%;
  top: 0;
  left: 0;
  z-index: 1;
  user-select: none;
`


export {
  AppFrameSty,

}