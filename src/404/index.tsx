import { useState, useEffect, useRef, useMemo, useCallback } from 'react'
import { useNavigate } from 'react-router-dom';
import { FrameSty } from './style';

export default function NotFound() {
  let navigate = useNavigate();

  useEffect(() => {
    setTimeout(() => {
      navigate('/')
    }, 1000)
  }, [])

  return (
    <FrameSty>
      <div className="error">404</div>
      <br /><br />
      <span className="info">即将跳转到首页</span>
      <img src="http://images2.layoutsparks.com/1/160030/too-much-tv-static.gif" className="static" />
    </FrameSty>
  )
}