import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { Outlet, useLocation, useNavigate } from 'react-router-dom'
import 'react-toastify/dist/ReactToastify.css';
import PageLoading from '@/components/PageLoading'
import Home from '@/pages/home'
import { AppFrameSty, HeaderSty } from './AppStyle';
import { SkipType } from './enum';
import UnderlineAnimation from './components/UnderlineAnimation';
import useSkip from '@/hooks/useSkip';

function App() {
  let navigate = useNavigate();
  let location = useLocation();
  let [sectionList, setSectionList] = useState([
    { name: '首页', SkipType: SkipType.HOME },
    { name: '功能Demo', SkipType: SkipType.FUN },
    { name: '文章', SkipType: SkipType.ARTICLE },
    { name: '关于', SkipType: SkipType.ABOUT },
  ])
  const { skipTo } = useSkip();
  /** 是否出现加载页面*/
  const [loading, setLoading] = useState(true);
  /** 导航颜色*/
  const navColor = useMemo(() => {
    let path = location.pathname;
    let condition: any = {
      '/': {
        color: '#fff',
        activeColor: '#ee5100'
      },
      '/funList': {
        color: '#000',
        activeColor: '#3498db'
      },
      '/article': {
        color: '#fff',
        activeColor: '#00FFDF'
      },
    }
    if (new RegExp("/exampleDetail").test(path)) {
      return {
        color: '#baa6cb',
        activeColor: '#086ACC'
      }
    }

    if (!condition[path]) {
      return {
        color: '#fff',
        activeColor: '#ee5100'
      }
    }
    return {
      color: condition[path].color,
      activeColor: condition[path].activeColor
    }
  }, [location])


  const headerStyle = useMemo(() => {
    let condition: {
      [key: string]: React.CSSProperties
    } = {
      '/': {
        background: 'transparent',
      },
      '/funList': {
        backdropFilter: 'blur(10px)',
      },
      '/article': {
        backdropFilter: 'blur(10px)',
      }
    }
    return condition[location.pathname] || {}

  }, [location])


  /** 有登录的话，检查登录状态*/
  const checkLongStatus = useCallback(() => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('success')
      }, 1000)
    })
  }, [])

  useEffect(() => {
    checkLongStatus().then((res) => {
      setLoading(false)
    })
  }, [])

  return (
    <>
      <AppFrameSty >
        <HeaderSty style={headerStyle}>
          {
            sectionList.map((item, index) => {
              return <UnderlineAnimation
                key={index}
                text={item.name}
                color={navColor.color}
                activeColor={navColor.activeColor}
                handelClick={() => {
                  skipTo(item.SkipType)
                }} />
            })
          }
        </HeaderSty>
        {/* 页面加载 */}
        {loading && <PageLoading />}
        <Outlet />
      </AppFrameSty>
    </>
  )
}
export default App
