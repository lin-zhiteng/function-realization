import WaterfallFlow from '@/pages/exampleList/waterfallFlow/index'
import VariableTypeEditing from '@/pages/exampleList/variableTypeEditing/index'
import Uploading from '@/pages/exampleList/uploading/index'
import UnequalHeightVirtualScroll from '@/pages/exampleList/unequalHeightVirtualScroll/index'
import Typewriter from '@/pages/exampleList/typewriter/index'
import TwoFactorAuth from '@/pages/exampleList/twoFactorAuth/index'
import Token from '@/pages/exampleList/token/index'
import Test from '@/pages/exampleList/test/index'
import RollingSuspension from '@/pages/exampleList/rollingSuspension/index'
import RichText from '@/pages/exampleList/richText/index'
import Print from '@/pages/exampleList/print/index'
import ParticleText from '@/pages/exampleList/particleText/index'
import OnlineEditing from '@/pages/exampleList/onlineEditing/index'
import NpmVirtualList from '@/pages/exampleList/npmVirtualList/index'
import MoreCascader from '@/pages/exampleList/MoreCascader/index'
import MessageNotification from '@/pages/exampleList/messageNotification/index'
import MagnifyingGlass from '@/pages/exampleList/magnifyingGlass/index'
import LazyImg from '@/pages/exampleList/lazyImg/index'
import GsapTextReveals from '@/pages/exampleList/gsapTextReveals/index'
import GsapInstep from '@/pages/exampleList/gsapInstep/index'
import GsapHorizontalScroll from '@/pages/exampleList/gsapHorizontalScroll/index'
import GlobalToast from '@/pages/exampleList/globalToast/index'
import EqualHeightVirtualScroll from '@/pages/exampleList/equalHeightVirtualScroll/index'
import EnlargePreview from '@/pages/exampleList/enlargePreview/index'
import Ellipsis from '@/pages/exampleList/ellipsis/index'
import DigitalRain from '@/pages/exampleList/digitalRain/index'
import ClockReturner from '@/pages/exampleList/clockReturner/index'
import BlobUse from '@/pages/exampleList/blobUse/index'
import App from '../App';
import Home from '../pages/home/index';
import ExampleDetail from '../pages/exampleDetail/index';
import FunList from '../pages/exampleList';
import Article from '../pages/article/index';
import About from '../pages/about/index';
import NotFound from '../404/index';
import { createBrowserRouter, createHashRouter, Navigate } from 'react-router-dom';

let childrenList = [{"title":"Canvas粒子文字","sortInd":"1","path":"particleText","element":<ParticleText />},{"title":"Blob的使用场景","path":"blobUse","sortInd":"28","element":<BlobUse />},{"title":"时钟翻牌器","path":"clockReturner","sortInd":"28","element":<ClockReturner />},{"title":"数字雨","path":"digitalRain","sortInd":"28","element":<DigitalRain />},{"title":"自适应省略号","path":"ellipsis","sortInd":"28","element":<Ellipsis />},{"title":"hook实现放大浏览","path":"enlargePreview","sortInd":"28","element":<EnlargePreview />},{"title":"等高虚拟滚动","path":"equalHeightVirtualScroll","sortInd":"28","element":<EqualHeightVirtualScroll />},{"title":"全局Toast","path":"globalToast","sortInd":"28","element":<GlobalToast />},{"title":"gasp横向滚动","path":"gsapHorizontalScroll","sortInd":"28","element":<GsapHorizontalScroll />},{"title":"gasp滚动渐入","path":"gsapInstep","sortInd":"28","element":<GsapInstep />},{"title":"gasp文字揭示","path":"gsapTextReveals","sortInd":"28","element":<GsapTextReveals />},{"title":"懒加载图片","path":"lazyImg","sortInd":"28","element":<LazyImg />},{"title":"放大镜","path":"magnifyingGlass","sortInd":"28","element":<MagnifyingGlass />},{"title":"消息通知","path":"messageNotification","sortInd":"28","element":<MessageNotification />},{"title":"多层级联级选择器","path":"MoreCascader","sortInd":"28","element":<MoreCascader />},{"title":"第三方虚拟列表","path":"npmVirtualList","sortInd":"28","element":<NpmVirtualList />},{"title":"在线编辑","path":"onlineEditing","sortInd":"28","element":<OnlineEditing />},{"title":"打印预览","path":"print","sortInd":"28","element":<Print />},{"title":"富文本","path":"richText","sortInd":"28","element":<RichText />},{"title":"滚动悬浮","path":"rollingSuspension","sortInd":"28","element":<RollingSuspension />},{"title":"测试","path":"test","sortInd":"28","element":<Test />},{"title":"Token刷新","path":"token","sortInd":"28","element":<Token />},{"title":"2FA","path":"twoFactorAuth","sortInd":"28","element":<TwoFactorAuth />},{"title":"多行打字机","path":"typewriter","sortInd":"28","element":<Typewriter />},{"title":"不等高虚拟列表","path":"unequalHeightVirtualScroll","sortInd":"28","element":<UnequalHeightVirtualScroll />},{"title":"图片上传","path":"uploading","sortInd":"28","element":<Uploading />},{"title":"数据类型编辑","path":"variableTypeEditing","sortInd":"28","element":<VariableTypeEditing />},{"title":"响应式瀑布流","path":"waterfallFlow","sortInd":"28","element":<WaterfallFlow />}];

let children = childrenList.map(item => {
  return {
    element: item.element,
    path: item.path,
  };
});

const router = createHashRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        path: '/',
        element: <Home />,
      },
      {
        path: 'exampleDetail',
        element: <ExampleDetail />,
        children,
      },
      {
        path: 'funList',
        element: <FunList />,
      },
      {
        path: 'article',
        element: <Article />,
      },
      {
        path: 'about',
        element: <About />,
      },
    ],
  },
  {
    path: '/404',
    element: <NotFound />,
  },
  {
    path: '*',
    element: <Navigate to="/404" />,
  },
]);

let routeList = childrenList;

export { routeList };

export default router;