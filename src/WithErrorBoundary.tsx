import { useState, useEffect, useRef, useMemo, useCallback } from 'react'
import ErrorBoundary from './ErrorBoundary'

export default function WithErrorBoundary(Component:any) {
  return (props: any) => { 
    return (
      <>
        <ErrorBoundary>
          <Component {...props}></Component>
        </ErrorBoundary>
      </>
    )
  }
}